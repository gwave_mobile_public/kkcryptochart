//
//  KKCryptoChartGlobalConfig.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/4.
//

#import "KKCryptoChartGlobalConfig.h"

@implementation KKCryptoChartGlobalConfig

- (instancetype)initWithLocale:(NSString *)locale timeType:(NSString *)timeType coinPrecision:(NSString *)coinPrecision tradeVolumePrecision:(NSString *)tradeVolumePrecision environment:(NSString *)environment coinCode:(NSString *)coinCode {
    self = [super init];
    if (self) {
        [self setUpTimeType:timeType];
        _coinPrecision = coinPrecision;
    	_tradeVolumePrecision = tradeVolumePrecision;
        _coinCode = coinCode;
        [self setUpLocale:locale];
        [self setUpEnvironment:environment];
    }
    return self;
}

- (void)setUpTimeType:(NSString *)timeType {
    _timeType = timeType;
}

- (void)setUpLocale:(NSString *)locale {
    if ([locale isEqualToString:@"hk"]) {
        _locale = KKCryptoChartLocaleTypeTraditionalChinese;
    } else if ([locale isEqualToString:@"zh"]) {
        _locale = KKCryptoChartLocaleTypeSimplifiedChinese;
    } else if ([locale isEqualToString:@"en"]) {
        _locale = KKCryptoChartLocaleTypeEnglish;
    } else {
        _locale = KKCryptoChartLocaleTypeTraditionalChinese;
    }
}

- (void)setUpEnvironment:(NSString *)environment {
    if ([environment isEqualToString:@"beta"]) {
        _environment = KKCryptoChartEnvironmentTypeBeta;
    } else if ([environment isEqualToString:@"prod"] || [environment isEqualToString:@"pro"] || [environment isEqualToString:@"prodgreen"]) {
        _environment = KKCryptoChartEnvironmentTypeProduct;
    } else {
        _environment = KKCryptoChartEnvironmentTypeBeta;
    }
}

@end
