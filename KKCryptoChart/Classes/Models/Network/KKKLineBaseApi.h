//
//  KKKLineBaseApi.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/3.
//

#import <Foundation/Foundation.h>
#import "KKCryptoChartGlobalConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface KKKLineBaseApi : NSObject

@property (nonatomic, assign) KKCryptoChartEnvironmentType environment;

- (NSString *)getCustomURL;

- (NSString *)getBaseURL;

- (NSString *)getCompletedURL;

@end

NS_ASSUME_NONNULL_END
