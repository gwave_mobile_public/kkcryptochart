//
//  KKKLineInitApi.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/3.
//

#import "KKKLineInitApi.h"

@implementation KKKLineInitApi

- (NSString *)getCompletedURL {
    NSString *completedURL = nil;
    completedURL = [NSString stringWithFormat:@"%@?", [self getBaseURL]];
    if (self.symbol) {
        completedURL = [NSString stringWithFormat:@"%@symbol=%@", completedURL, self.symbol];
    }
    if (self.resolution) {
        completedURL = [NSString stringWithFormat:@"%@&resolution=%@", completedURL, self.resolution];
    }
    if (self.from) {
        completedURL = [NSString stringWithFormat:@"%@&from=%@", completedURL, self.from];
    }
    if (self.to) {
        completedURL = [NSString stringWithFormat:@"%@&to=%@", completedURL, self.to];
    }
    return completedURL;
}

@end
