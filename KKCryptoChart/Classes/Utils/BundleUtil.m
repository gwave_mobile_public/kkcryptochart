//
//  BundleUtil.m
//  KKCryptoChart
//
//  Created by apple on 2021/3/16.
//

#define BUNDLE_NAME @"KKCryptoChartResource"

#import "BundleUtil.h"
#import "KKCryptoChartConstant.h"
#import "KKCryptoChart.h"

@implementation BundleUtil

+ (NSBundle *)getBundle {
//    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.bundle",BUNDLE_NAME]];
//    KKLog(@"BundleUtil getBundle path=%@", path);
//    return [NSBundle bundleWithPath:path];
#ifdef SWIFT_PACKAGE
    NSBundle *bundle = SWIFTPM_MODULE_BUNDLE;
#else
    NSBundle *bundle = [NSBundle bundleForClass:[KKCryptoChartView class]];
#endif
    NSURL *url = [bundle URLForResource:BUNDLE_NAME withExtension:@"bundle"];
    bundle = [NSBundle bundleWithURL:url];
    return bundle;
}

+ (NSString *)getFilePathFromBundle:(NSString *)filePathInBundle {
    
    NSBundle *myBundle = [BundleUtil getBundle];
    if (myBundle && filePathInBundle) {
        return [[myBundle resourcePath] stringByAppendingPathComponent:filePathInBundle];
    }
    return nil;
}

@end
