//
//  KKCryptoChartDescription.m
//  KikiChartsDemo
//
//  Created by apple on 2021/3/3.
//

#import "KKCryptoChartDescription.h"
#import "KKCryptoChartConstant.h"
#import "KKCryptoChartModel.h"
#import "KKCryptoChartGlobalConfig.h"
#import "KKCryptoChartDataManager.h"

@interface KKCryptoChartDescription ()

@property (nonatomic, strong) UILabel *coinCodeLabel;
@property (nonatomic, strong) UILabel *openLabel;
@property (nonatomic, strong) UILabel *closeLabel;
@property (nonatomic, strong) UILabel *highLabel;
@property (nonatomic, strong) UILabel *lowLabel;
@property (nonatomic, strong) KKCryptoChartGlobalConfig *config;

@end

@implementation KKCryptoChartDescription

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI:frame];
    }
    return self;
}

- (void)initUI:(CGRect)frame {
    self.coinCodeLabel = [[UILabel alloc] init];
    self.coinCodeLabel.font = [UIFont boldSystemFontOfSize:14];
    [self addSubview:self.coinCodeLabel];
    
    self.openLabel = [[UILabel alloc] init];
    self.openLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.openLabel];
    
    self.highLabel = [[UILabel alloc] init];
    self.highLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.highLabel];
    
    self.lowLabel = [[UILabel alloc] init];
    self.lowLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.lowLabel];
    
    self.closeLabel = [[UILabel alloc] init];
    self.closeLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.closeLabel];
}

- (void)layoutSubviews {
    [super layoutSubviews];
//    KKLog(@"KKCryptoChartDescription layoutSubviews x=%f, y=%f, width=%f, height=%f", self.frame.origin.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    [self.coinCodeLabel setFrame:CGRectMake(0, 0, self.frame.size.width, 17)];
    [self.openLabel setFrame:CGRectMake(0, CGRectGetMaxY(self.coinCodeLabel.frame) + 4, self.openLabel.frame.size.width, 14)];
    [self.highLabel setFrame:CGRectMake(CGRectGetMaxX(self.openLabel.frame) + 10, self.openLabel.frame.origin.y, self.highLabel.frame.size.width, 14)];
    [self.lowLabel setFrame:CGRectMake(CGRectGetMaxX(self.highLabel.frame) + 10, self.openLabel.frame.origin.y, self.lowLabel.frame.size.width, 14)];
    [self.closeLabel setFrame:CGRectMake(0, CGRectGetMaxY(self.openLabel.frame) + 4, self.closeLabel.frame.size.width, 14)];
}

- (NSArray<NSString *> *)getLocaleStrings:(KKCryptoChartGlobalConfig *)config {
    return @[
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_desc_open"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_desc_high"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_desc_low"],
        [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:@"chart_desc_close"]
    ];
}

- (NSString *)getLocaleStringFromManager:(NSString *)key {
    return [[KKCryptoChartDataManager shareManager] getLocalizableStringWithKey:key];
}

- (void)setUpWithChartModel:(KKCryptoChartModel *)model config:(KKCryptoChartGlobalConfig *)config {
    if (!model || !config) {
        return;
    }
    _config = config;
    [self refreshLabelUIWithChartModel:model config:config];
}

- (void)refreshLabelUIWithChartModel:(KKCryptoChartModel *)model config:(KKCryptoChartGlobalConfig *)config {
    self.coinCodeLabel.text = [NSString stringWithFormat:@"%@,%@", [model.coinCode stringByReplacingOccurrencesOfString:@"_" withString:@"/"], model.timeType];
    UIColor *numColor = model.open.doubleValue > model.close.doubleValue ? KK_CRYPTO_CHART_CANDLE_DECREASE_COLOR : KK_CRYPTO_CHART_CANDLE_INCREASE_COLOR;
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setMaximumFractionDigits:config.coinPrecision.integerValue];
    [formatter setMinimumFractionDigits:config.coinPrecision.integerValue];

    NSString *open = [NSString stringWithFormat:@"%@=%@", [self getLocaleStrings:config][0], [formatter stringFromNumber:model.open]];
    self.openLabel.attributedText = [self setUpLabelAttributedString:open numColor:numColor];
    [self.openLabel sizeToFit];
    
    NSString *high = [NSString stringWithFormat:@"%@=%@", [self getLocaleStrings:config][1], [formatter stringFromNumber:model.high]];
    self.highLabel.attributedText = [self setUpLabelAttributedString:high numColor:numColor];
    [self.highLabel sizeToFit];

    NSString *low = [NSString stringWithFormat:@"%@=%@", [self getLocaleStrings:config][2], [formatter stringFromNumber:model.low]];
    self.lowLabel.attributedText = [self setUpLabelAttributedString:low numColor:numColor];
    [self.lowLabel sizeToFit];
    
    NSString *close = [NSString stringWithFormat:@"%@=%@", [self getLocaleStrings:config][3], [formatter stringFromNumber:model.close]];
    self.closeLabel.attributedText = [self setUpLabelAttributedString:close numColor:numColor];
    [self.closeLabel sizeToFit];

    [self setNeedsLayout];
}

- (NSMutableAttributedString *)setUpLabelAttributedString:(NSString *)string numColor:(UIColor *)numColor {
    if (string == nil || string.length < 2) {
        return nil;
    }
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:string];
    NSArray<NSString *> *splitStrings = [string componentsSeparatedByString:@"="];
    if (splitStrings.count == 2) {
        [attString addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:10] range:NSMakeRange(0, splitStrings[0].length)];
        [attString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:62/255.f green:75/255.f blue:95/255.f alpha:1.f] range:NSMakeRange(0, splitStrings[0].length)];
        [attString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:9] range:NSMakeRange(splitStrings[0].length, string.length - splitStrings[0].length)];
        [attString addAttribute:NSForegroundColorAttributeName value:numColor range:NSMakeRange(splitStrings[0].length, string.length - splitStrings[0].length)];
    }
    return attString;
}

- (void)updateDescription:(KKCryptoChartModel *)model {
    if (!model || !self.config) {
        return;
    }
    [self refreshLabelUIWithChartModel:model config:self.config];
}

@end
