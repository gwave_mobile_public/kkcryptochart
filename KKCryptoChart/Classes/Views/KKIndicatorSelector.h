//
//  KKIndicatorSelector.h
//  KikiChartsDemo
//
//  Created by apple on 2021/3/5.
//

#import <UIKit/UIKit.h>
#import "KKIndicatorModel.h"
@class TabbarSelectUtil;

NS_ASSUME_NONNULL_BEGIN

@protocol KKIndicatorSelectorDelegate <NSObject>

@optional

// main chart selector changed
- (void)indicatorSelectorMainChartValueChanged:(KKIndicator)indicatorType;

// side chart selector changed
- (void)indicatorSelectorSideChartValueChanged:(NSArray *)indicatorArray;

@end

@interface KKIndicatorSelector : UIView

@property (nonatomic, assign) id<KKIndicatorSelectorDelegate> delegate;

- (void)setUpData;

@end

NS_ASSUME_NONNULL_END
