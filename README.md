# KKCryptoChart

[![CI Status](https://img.shields.io/travis/HenryDang/KKCryptoChart.svg?style=flat)](https://travis-ci.org/HenryDang/KKCryptoChart)
[![Version](https://img.shields.io/cocoapods/v/KKCryptoChart.svg?style=flat)](https://cocoapods.org/pods/KKCryptoChart)
[![License](https://img.shields.io/cocoapods/l/KKCryptoChart.svg?style=flat)](https://cocoapods.org/pods/KKCryptoChart)
[![Platform](https://img.shields.io/cocoapods/p/KKCryptoChart.svg?style=flat)](https://cocoapods.org/pods/KKCryptoChart)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

KKCryptoChart is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'KKCryptoChart'
```

## Author

HenryDang, henry.dang@kikitrade.com

## License

KKCryptoChart is available under the MIT license. See the LICENSE file for more info.
